import static org.junit.Assert.*;

import org.junit.Test;

public class productTest {

	
	/*
	 * TC1: Creating a Product
     *		When you create the product, it has a title and a price
	 *
	 * TC2: Checking if two products are equal
	 * 		Two products are equal if they have the same name.
	 */
	@Test
	public void testCreateProduct() {
		// 1. Create a new Product
		Product p= new Product("Chair",40);
				// 2. Check that product name = expected name
		assertEquals("Chair",p.getTitle());
		assertEquals(40,p.getPrice(),0.01);
				// 3. Check that product price = expected price
		
		
	}
	@Test
	public void testTwoProductsEqual() {
		Product p = new Product("Table",50);
		Product p1 = new Product("Table",80);
		assertEquals(true,p1.equals(p));
	}

}
