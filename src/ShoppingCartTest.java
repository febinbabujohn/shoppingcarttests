import static org.junit.Assert.*;

import org.junit.Test;

public class ShoppingCartTest {

	@Test
	public void CreatingaCart() {
		//When created the cart has 0 items
		//1. Creating a shopping cart
		ShoppingCart cart=new ShoppingCart();
		
		//Use getItemCount() to check if it is empty
		int numitems=cart.getItemCount();
		assertEquals(0,numitems);
	}
	@Test
	public void EmptyCarts() {
		//Create a Cart
		Product p=new Product("Coffee",3);
		Product p1=new Product("TimBits",1);
		ShoppingCart cart=new ShoppingCart();
		//Add things to a cart
		cart.addItem(p);
		cart.addItem(p1);
		
		//Empty the Cart
		cart.empty();
		//Check the number of items in the Cart
		assertEquals(0,cart.getItemCount());
	}
}
